"""
Utility module to run elevated processes.
"""

import sys
import ctypes
from ctypes import sizeof, byref

from devdis.exceptions import WindowsSystemError


_shell32 = ctypes.windll.shell32



### WINDOWS DATA TYPES ########################################################

from ctypes.wintypes import (
    BOOL,
    DWORD,
    ULONG,
    HWND,
    LPCWSTR,
    LPVOID,
    HINSTANCE,
    HKEY,
    HANDLE
)



### FLAGS #####################################################################

SW_FORCEMINIMIZE = 11
SW_HIDE = 0
SW_MAXIMIZE = 3
SW_MINIMIZE = 6
SW_RESTORE = 9
SW_SHOW = 5
SW_SHOWDEFAULT = 10
SW_SHOWMAXIMIZED = 3
SW_SHOWMINIMIZED = 2
SW_SHOWMINNOACTIVE = 7
SW_SHOWNA = 8
SW_SHOWNOACTIVATE = 4
SW_SHOWNORMAL = 1

SEE_MASK_NOCLOSEPROCESS = 0x00000040
SEE_MASK_INVOKEIDLIST = 0x0000000C



### DATA STRUCTURES ###########################################################

class SHELLEXECUTEINFO(ctypes.Structure):
    _fields_ = [
        ("cbSize", DWORD),
        ("fMask", ULONG),
        ("hwnd", HWND),
        ("lpVerb", LPCWSTR),
        ("lpFile", LPCWSTR),
        ("lpParameters", LPCWSTR),
        ("lpDirectory", LPCWSTR),
        ("nShow", ctypes.c_int),
        ("hInstApp", HINSTANCE),
        ("lpIDList", LPVOID),
        ("lpClass", LPCWSTR),
        ("hkeyClass", HKEY),
        ("dwHotKey", DWORD),
        ("hIconOrMonitor", HANDLE),
        ("hProcess", HANDLE)
    ]

LPSHELLEXECUTEINFO = ctypes.POINTER(SHELLEXECUTEINFO)



### PYTHON API ################################################################

def is_admin():
    return _shell32.IsUserAnAdmin()


def run_elevated(exe, param=None):
    """Run *exe* with *param* using elevated credentials. Returns
    immediately after the executable has been called.

    Raises :exc:`WindowsSystemError` if the call failed (such as when
    the user denies elevation).
    """

    execute = SHELLEXECUTEINFO()
    execute.cbSize = sizeof(SHELLEXECUTEINFO)
    execute.lpVerb = "runas" if not is_admin() else None
    execute.lpFile = exe
    execute.lpParameters = param
    execute.lpDirectory = None
    execute.nShow = SW_SHOWNORMAL

    if not _shell32.ShellExecuteExW(byref(execute)):
        raise WindowsSystemError()


def elevate_script():
    """Re-run the current executing Python script with elevated
    credentials. The script will be re-executed using the command and
    arguments from ``sys.argv``. Once the new elevated process
    successfully starts, then the current script will forcibly terminate
    with ``exit(0)``.

    Raises :exc:`WindowsSystemError` on failure.
    """

    # Wrap all cmdline parameters with quotes before joining.
    param = " ".join('"{}"'.format(s) for s in sys.argv)
    run_elevated(sys.executable, param)
    exit(0)