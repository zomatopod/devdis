"""
Utilty API that defines a few Python bindings to manipulate Windows
devices using the Windows Setup API. Current functionality exists to
enumerate devices and their instance IDs, and disabling/enabling
devices.
"""

# The Unicode versions of Setup API are used.

import contextlib
import enum
import ctypes
from ctypes import create_unicode_buffer, byref, sizeof, memmove, GetLastError

from devdis.exceptions import WindowsSystemError


_setupapi = ctypes.windll.setupapi



### WINDOWS DATA TYPES ########################################################

from ctypes.wintypes import (
    BOOL,
    BYTE,
    UINT,
    WORD,
    DWORD,
    PDWORD,
    ULONG,
    LPWSTR
)

ULONG_PTR = ctypes.POINTER(ULONG)



### SETUPAPI FLAGS AND CONSTANTS ##############################################

DIGCF_ALLCLASSES = 0x4
DIGCF_PRESENT = 0x2

DICS_ENABLE = 0x1
DICS_DISABLE = 0x2
DICS_FLAG_GLOBAL = 0x1

DIF_PROPERTYCHANGE = 0x12

ERROR_INSUFFICIENT_BUFFER = 122



### SETUPAPI STRUCTS AND DATA TYPES ###########################################

HDEVINFO = ctypes.c_void_p
DI_FUNCTION = UINT


class GUID(ctypes.Structure):
    _fields_ = [
        ('Data1', DWORD),
        ('Data2', WORD),
        ('Data3', WORD),
        ('Data4', BYTE*8),
    ]


class SP_DEVINFO_DATA(ctypes.Structure):
    _fields_ = [
        ("cbSize", DWORD),
        ("ClassGuid", GUID),
        ("DevInst", DWORD),
        ("Reserved", ULONG_PTR)
    ]


class SP_CLASSINSTALL_HEADER(ctypes.Structure):
    _fields_ = [
        ("cbSize", DWORD),
        ("InstallFunction", DI_FUNCTION)
    ]


class SP_PROPCHANGE_PARAMS(ctypes.Structure):
    _fields_ = [
        ("ClassInstallHeader", SP_CLASSINSTALL_HEADER),
        ("StateChange", DWORD),
        ("Scope", DWORD),
        ("HwProfile", DWORD)
    ]


PSP_DEVINFO_DATA = ctypes.POINTER(SP_DEVINFO_DATA)
PSP_CLASSINSTALL_HEADER = ctypes.POINTER(SP_CLASSINSTALL_HEADER)



### FOREIGN API ###############################################################

SetupDiGetClassDevs = _setupapi.SetupDiGetClassDevsW
SetupDiDestroyDeviceInfoList = _setupapi.SetupDiDestroyDeviceInfoList

SetupDiEnumDeviceInfo = _setupapi.SetupDiEnumDeviceInfo
SetupDiEnumDeviceInfo.restype = BOOL
SetupDiEnumDeviceInfo.argtypes = [HDEVINFO, DWORD, PSP_DEVINFO_DATA]

SetupDiGetDeviceInstanceId = _setupapi.SetupDiGetDeviceInstanceIdW
SetupDiGetDeviceInstanceId.restype = BOOL
SetupDiGetDeviceInstanceId.argtypes = [HDEVINFO, PSP_DEVINFO_DATA, LPWSTR,
                                       DWORD, PDWORD]

SetupDiSetClassInstallParams = _setupapi.SetupDiSetClassInstallParamsW
SetupDiSetClassInstallParams.restype = BOOL
SetupDiSetClassInstallParams.argtypes = [HDEVINFO, PSP_DEVINFO_DATA,
                                         PSP_CLASSINSTALL_HEADER, DWORD]

SetupDiCallClassInstaller = _setupapi.SetupDiCallClassInstaller
SetupDiCallClassInstaller.restype = BOOL
SetupDiCallClassInstaller.argtypes = [DI_FUNCTION, HDEVINFO, PSP_DEVINFO_DATA]



### PYTHON API ################################################################

class DevInfo(object):
    """:class:`DevInfo` wraps the Setup API ``HDEVINFO`` handle with
    a set of methods for device operations.

    All parameters are optional. If *flags* is omitted, then
    `DIGCF_ALLCLASSES | DIGCF_PRESENT`` will be used.
    """

    def __init__(self, flags=None, class_guid=None, enumerator=None):
        self._hdevinfo = SetupDiGetClassDevs(class_guid,
                                             enumerator,
                                             None,
                                             flags)

    @property
    def hdevinfo(self):
        """The raw ``HDEVINFO`` handle."""
        return self._hdevinfo

    def destroy(self):
        """Destroy the handle associated with the :class:`DevInfo`
        object. The handle should always be destroyed when you are
        finished using it. For a more robust method of ensuring cleanup,
        use the :meth:`new` context manager instead of calling
        :meth:`destroy` directly.
        """

        if self._hdevinfo:
            SetupDiDestroyDeviceInfoList(self._hdevinfo)

    @staticmethod
    @contextlib.contextmanager
    def new(flags=None, class_guid=None, enumerator=None):
        """A context manager that creates a :class:`DevInfo` object and
        destroys it when the context block ends.
        """

        if flags is None:
            flags = DIGCF_ALLCLASSES | DIGCF_PRESENT

        handle = DevInfo(flags, class_guid, enumerator)

        try:
            yield handle
        finally:
            handle.destroy()

    def enum_device_info(self):
        """Generator to enumerate devices. Yields :class:`DevInfoData`.
        """

        devinfo_data = SP_DEVINFO_DATA()
        devinfo_data.cbSize = sizeof(SP_DEVINFO_DATA)

        req_size = DWORD(128)
        inst_id = create_unicode_buffer(req_size.value)

        index = 1

        while SetupDiEnumDeviceInfo(
                self._hdevinfo,
                DWORD(index),
                devinfo_data
            ):
            # Get instance id string.
            if not SetupDiGetDeviceInstanceId(
                      self._hdevinfo,
                      devinfo_data,
                      inst_id,
                      DWORD(len(inst_id)),
                      byref(req_size)
                ):
                # Expand buffer to fit the required size and retry.
                if GetLastError() == ERROR_INSUFFICIENT_BUFFER:
                    inst_id = create_unicode_buffer(req_size.value)
                    continue
                # Ignore errors for now.
                else:
                    index += 1
                    continue

            yield DevInfoData(devinfo_data, inst_id.value, self)
            index += 1


class DeviceState(enum.Enum):
    enable = DICS_ENABLE
    disable = DICS_DISABLE


class DevInfoData(object):
    """:class:`DevInfo` wraps the Setup API ``SP_DEVINFO_DATA`` struct
    with a set of methods for device operations.
    """

    def __init__(self, devinfo_data, instance_id, handle):
        # Make an instance copy of the devinfo data instead of keeping
        # a reference, which may be mutated after this object has been
        # instantated (such as in the DevInfo.enum_device_info()
        # generator).
        self.devinfo_data = SP_DEVINFO_DATA()
        memmove(byref(self.devinfo_data),
                byref(devinfo_data),
                sizeof(devinfo_data))

        self.instance_id = instance_id
        self._hdevinfo = handle.hdevinfo

    def __getattr__(self, key):
        return getattr(self.devinfo_data, key)

    def state(self, state):
        """Enable or disable the device. *state* is the
        :class:`DeviceState` to set the device to. This operation
        requires elevated permissions to successfully execute.

        Raises :exc:`WindowsSystemError` if the operation failed.
        """

        inst_params = SP_PROPCHANGE_PARAMS()
        inst_params.ClassInstallHeader.cbSize = sizeof(SP_CLASSINSTALL_HEADER)
        inst_params.ClassInstallHeader.InstallFunction = DIF_PROPERTYCHANGE
        inst_params.Scope = DICS_FLAG_GLOBAL
        inst_params.StateChange = state.value

        if not SetupDiSetClassInstallParams(
                self._hdevinfo,
                self.devinfo_data,
                byref(inst_params.ClassInstallHeader),
                sizeof(inst_params)
            ):
            raise WindowsSystemError()

        if not SetupDiCallClassInstaller(
                DIF_PROPERTYCHANGE,
                self._hdevinfo,
                self.devinfo_data
            ):
            raise WindowsSystemError()
