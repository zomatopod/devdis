import ctypes
from ctypes import byref
from ctypes.wintypes import DWORD, LPWSTR


class WindowsSystemError(OSError):
    """A Windows system error from the Windows API as returned by
    GetLastError(). Inherits from :exc:`OSError`.

    Takes the an optional numeric Windows error *code*. If omitted,
    ``GetLastError()`` will be called to try to obtain the last error.
    """

    def __init__(self, code=None):
        self.code = code if code is not None else ctypes.GetLastError()

        # Get a descriptive message associated with the code.
        str_buf = LPWSTR()
        if ctypes.windll.kernel32.FormatMessageW(
                0x1100, # message from system + allocate buffer
                None,
                DWORD(self.code),
                0,
                byref(str_buf),
                0,
                None
            ):
            # Strip trailing newline.
            msg = str(str_buf.value).strip()
        else:
            msg = "Unknown error."

        super().__init__("{} ({}): {}".format(self.code, hex(self.code), msg))
        ctypes.windll.kernel32.LocalFree(str_buf)
