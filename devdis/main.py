import subprocess
import os
import time
import contextlib

import click

from devdis import setupapi
from devdis import runas


@click.command()
@click.argument('instance_id', type=str)
@click.argument('exe', type=click.Path(exists=True))
@click.argument('exe_args', nargs=-1)
@click.option('--poll', is_flag=True,
              help="Instead of ending as soon as the process terminates, "
                   "continuously poll its image name to determine its status. "
                   "This is necessary for Steam games, which uses a stub "
                   "launcher to start the actual game process from the Steam "
                   "client.")
@click.option('--wait', default=10,
              help="Seconds to wait after launching the exe before monitoring "
                   "the process's image name. Only applicable with --poll. "
                   "Defaults to 10.")
def entry(instance_id, exe, exe_args, poll, wait):
    """Disable the device of INSTANCE_ID, run the EXE with EXE_ARGS, and
    then re-enable the device when the called application finishes.
    """

    exe_cmd = '"{}" {}'.format(exe, " ".join(exe_args))

    # Re-invoke script with admin permissions.
    if not runas.is_admin():
        runas.elevate_script()

    with setupapi.DevInfo.new() as handle:
        device = match_inst_id(handle, instance_id)
        with toggle_device_state(device):
            if poll:
                subprocess.Popen(exe)
                time.sleep(wait)
                block_exec(exe)
            else:
                subprocess.call(exe)


def match_inst_id(handle, instance_id):
    """Return a :class:`DevInfoData` that matches *instance_id* string.
    Raises :exc:`ValueError` if no device was found.
    """

    for device_data in handle.enum_device_info():
        if instance_id == device_data.instance_id:
            return device_data
    raise ValueError("Instance ID {} not found".format(instance_id))


@contextlib.contextmanager
def toggle_device_state(device_data):
    """Disable a device and then re-enable it when the context block
    ends.
    """

    device_data.state(setupapi.DeviceState.disable)
    try:
        yield
    finally:
        device_data.state(setupapi.DeviceState.enable)


def block_exec(exe):
    """Block until the *exe* process is no longer running. *exe* may
    be a full or partial path, but must have an appropriate executable
    extension or else :exc:`ValueError` will be raised.
    """

    if not any(exe.endswith(ext) for ext in (".exe", ".bat", ".com", ".cmd")):
        raise ValueError("Executable target needs an appropriate extension.")

    image_name = os.path.split(exe)[1]
    cmd = 'tasklist /fi "IMAGENAME eq {}"'.format(image_name)

    # tasklist command outputs an error message when a filter fails,
    # meaning our target image isn't running.
    no_task = b"No tasks are running"

    while True:
        if no_task in subprocess.check_output(cmd):
            return
        time.sleep(1)
