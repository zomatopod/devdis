import os
import sys
from setuptools import setup, find_packages, Command

import devdis


def read(fn):
    cwd = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(cwd, fn), 'r') as file_:
        return file_.read()

setup(
    name="devdis",
    version=devdis.__version__,
    author="Lindy Meas",
    description="Windows device disabler.",
    long_description=read("README.rst"),
    packages=find_packages(),
    install_requires=[
        "click>=2.0"
    ],
    entry_points={
        'console_scripts': 'devdis = devdis.main:entry'
    }
)