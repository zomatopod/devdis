devdis
======

Dark Souls II has a problem where controller support dies if it there's any HID
device present besides a 360 controller. This was also a problem in Dark Souls I
and they apparently couldn't be bothered to fix it.

Their official solution is to unplug or disable the devices before you run the
game. That's stupid. And I'm really lazy.

So this thing does it for me.